ANALYSE IPL DATASET USING PYTHON AND MATPLOTLIB

In this data project we have two csv files matches.csv and deliveries.csv containing data about matches played and deliveries in IPL and by using the data and transforming them into functions the data is plotted into graphs displaying a useful information.


IMPLEMENTED SCENERIOS:

1. Plotted number of matches played per season using bar chart.
2. Plotted a stack bar chart of each team's win in the years using stacked bar charts.
3. Plotted a bar chart of the extra runs of each team in the year 2016.
4. Plotted a bar chart of the top 15 economical bowlers in the year 2015.
