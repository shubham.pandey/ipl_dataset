import csv
from matplotlib import pyplot as plt
from common import *

deliveries_file = 'deliveries.csv'
filename = 'matches.csv'
season = input("Enter season : ")


def read_deliveries(name, year_to_match):
    id_list = list(read_matches(filename, year_to_match))
    batsmans = {}
    with open(name) as deliveries_file:
        deliveries_data = csv.DictReader(deliveries_file)
        for deliveries in deliveries_data:
            batsman_name = deliveries['batsman']
            batsman_run = deliveries['batsman_runs']
            if deliveries['match_id'] in id_list:
                if batsman_run == "6":
                    if batsman_name not in batsmans:
                        batsmans[deliveries['batsman']] = {
                            "sixes": 0
                        }
                    batsmans[batsman_name]['sixes'] += 1
    return batsmans


delivery_data = read_deliveries(deliveries_file, season)


def sorting_top_15():
    top15_batsmans = delivery_data
    batsman_list = []
    for name, sixes in sorted(top15_batsmans.items()):
        batsman_list.append(top15_batsmans[name]['sixes'])
    batsman_list.sort(reverse=True)
    batsman_list = batsman_list[:15]
    data_to_plot = {}
    for batsman, sixes in sorted(top15_batsmans.items()):
        for name, count in sorted(sixes.items()):
            if name == "sixes" and count in batsman_list:
                data_to_plot[batsman] = count
    return data_to_plot


def plot_data():
    sixes_count = sorting_top_15()
    x_coordinate = []
    y_coordinate = []
    for name, sixes in sorted(sixes_count.items(), key=lambda x: x[1]):
        y_coordinate.append(round(sixes, 2))
        x_coordinate.append(name)
    plt.bar(x_coordinate, y_coordinate, width=0.5, color=['blue', 'red'])
    plt.xticks(rotation="45")
    plt.xlabel("Batsman")
    plt.ylabel("Economy")
    plt.tight_layout()
    plt.show()
    print(sixes_count)


plot_data()
