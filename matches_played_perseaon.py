import csv
from collections import Counter
from matplotlib import pyplot as plt
filename = 'matches.csv'


def read_matches(name):
    with open(name)as matches:
        match_dictionary = csv.DictReader(matches)
        season_list_ = []
        season_list_ = [matches['season'] for matches in match_dictionary]
        return season_list_


def count_matches_played():
    list_matches = read_matches(filename)
    matches_per_season = Counter(list_matches)
    x = [i for i in sorted(matches_per_season.keys())]
    y = [matches_per_season[i] for i in sorted(matches_per_season.keys())]
    plt.bar(x, y, color=['yellow', 'blue'])
    plt.show()


def execute():
    count_matches_played()


execute()
