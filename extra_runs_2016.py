import csv
from matplotlib import pyplot as plt
from common import *

match_file = 'matches.csv'
deliveries_file = 'deliveries.csv'
year = "2016"
list_2016_matches = list(read_matches(match_file, year))


def read_deliveries(deliveries_file, year):
    teams = {}
    with open(deliveries_file) as deliveries_file:
        deliveries_data = csv.DictReader(deliveries_file)
        for deliveries in deliveries_data:
            if deliveries['match_id'] in list_2016_matches:
                bowling_team = deliveries['bowling_team']
                extra_runs = int(deliveries['extra_runs'])
                if bowling_team not in teams:
                    teams[bowling_team] = 0
                else:
                    teams[bowling_team] += extra_runs
        return teams


extra_runs_per_team_2016 = read_deliveries(deliveries_file, "2016")


def plot_data():
    team_x_coordinate = []
    extra_runs_coordinate = []
    plot_runs = extra_runs_per_team_2016
    for team, runs in sorted(plot_runs.items(), key=lambda x: x[1]):
        if runs != 0:
            team_x_coordinate.append(team)
            extra_runs_coordinate.append(runs)
    plt.bar(team_x_coordinate, extra_runs_coordinate,
            width=0.5, color=['blue', 'red'])
    plt.xticks(rotation="50")
    plt.tight_layout()
    plt.show()


plot_data()
