import csv

FILENAME = 'matches.csv'


def read_matches(name, year_to_calculate):
    with open(name) as matches_file:
        # list_result_ = []
        matches_data = csv.DictReader(matches_file)
        for year in matches_data:
            season = year['season']
            id_season = year['id']
            if season == year_to_calculate:
                yield id_season
                # list_result_.append(id_season)
    # return list_result_
