import csv
from matplotlib import pyplot as plt
from common import*
import copy
filename = 'matches.csv'


def read_matches_data(name):
    with open(name) as matches:
        matches_reader = csv.DictReader(matches)
        # teams = team_dictionary()
        teams_dict = {}
        won_matches_per_team = dict()
        for seasons in matches_reader:
            teams_dict[seasons['team1']] = 0
            season = seasons['season']
            winner = seasons['winner']
            if season != 'season':
                if season not in won_matches_per_team.keys():
                    won_matches_per_team[season] = copy.copy(teams)
                if winner == "":
                    continue
                won_matches_per_team[season][winner] += 1
    return won_matches_per_team


def final_list_to_plot():
    won_dictionary = read_matches_data(filename)
    years = {years for years in won_dictionary.keys()}
    year_list_parent = []
    for year, win in sorted(won_dictionary.items()):
        team_win_list = []
        for team, team_win in sorted(win.items()):
            team_win_list.append(team_win)
        year_list_parent.append(team_win_list)
    years = list(years)
    years.sort()
    return [year_list_parent, years]


def bar_details():
    years_detail_list = final_list_to_plot()
    years = years_detail_list[1]
    matches_woncount = years_detail_list[0]
    return [matches_woncount, years]


def plot_data():
    bar = bar_details()
    win_count_per_team = bar[0]
    team_labels = bar[1]
    bottom = [0]*14
    N = 14
    width = 0.7
    xvalues = [i for i in range(N)]
    for index in win_count_per_team:
        plt.bar(xvalues, index, width=width,
                bottom=bottom)
        bottom = [bottom[i]+index[i] for i in range(len(bottom))]
    plt.xticks(xvalues, ('Chennai Super Kings', 'Deccan Chargers', 'Delhi Daredevils', 'Gujarat Lions', 'Kings XI Punjab', 'Kochi Tuskers Kerala', 'Kolkata Knight Riders',
                         'Mumbai Indians', 'Pune Warriors', 'Rajasthan Royals', 'Rising Pune Supergiant', 'Rising Pune Supergiants', 'Royal Challengers Bangalore', 'Sunrisers Hyderabad'), fontsize=10, rotation=45)
    plt.ylabel('Wins per year')
    plt.xlabel('Teams')
    plt.legend(team_labels)
    plt.tight_layout()
    plt.show()


def execute():
    plot_data()


execute()
