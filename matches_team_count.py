import csv
import matplotlib.pyplot as plt

# reads the data and prepares the dictionary of teams per year


def read_data():
    with open('matches.csv') as matches_information:
        teams_win_details_years_wise = {}
        year_list = []
        for year in range(2008, 2018):
            year_list.append(str(year))
        matches_data = csv.DictReader(matches_information)
        for match in matches_data:
            winner = match['winner']
            year = match['season']
            if winner == "":
                continue
            elif winner in teams_win_details_years_wise:
                if year in teams_win_details_years_wise[winner]:
                    teams_win_details_years_wise[winner][year] += 1
                else:
                    teams_win_details_years_wise[winner][year] = 1
            else:
                teams_win_details_years_wise[winner] = {year: 0}
            for year in year_list:
                if year not in teams_win_details_years_wise[winner]:
                    teams_win_details_years_wise[winner][year] = 0
    return teams_win_details_years_wise

# this prepares a list inside list to plot


def final_list_to_plot():
    win_details_per_year = read_data()
    teams = {teams for teams in win_details_per_year.keys()}
    team_list_parent = []
    for team, win in sorted(win_details_per_year.items()):
        year_win_list = []
        for year, year_win in sorted(win.items()):
            year_win_list.append(year_win)
        team_list_parent.append(year_win_list)
    teams = list(teams)
    teams.sort()
    return [team_list_parent, teams]

# this tells the detail to the bar


def bar_details():
    team_detail_list = final_list_to_plot()
    teams = team_detail_list[1]
    matches_woncount = team_detail_list[0]
    return [matches_woncount, teams]

# this plots the data


def plot_data():
    bar = bar_details()
    win_count_per_year = bar[0]
    team_lables = bar[1]
    bottom = [0]*10
    n_xrange = 10
    width = 0.7
    xvalues = [i for i in range(n_xrange)]
    for index in win_count_per_year:
        plt.bar(xvalues, index, width=width,
                bottom=bottom)
        bottom = [bottom[i]+index[i] for i in range(len(bottom))]
    plt.xticks(xvalues, ('2008', '2009', '2010', '2011', '2012', '2013',
                         "2014", "2015", "2016", "2017"), fontsize=10, rotation=45)
    plt.ylabel('Wins')
    plt.xlabel('Years')
    plt.legend(team_lables)
    plt.tight_layout()
    plt.show()


def execute():
    plot_data()


execute()
