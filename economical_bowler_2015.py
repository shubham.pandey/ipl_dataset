import csv
from matplotlib import pyplot as plt
from common import *

filename = 'matches.csv'
filename2 = 'deliveries.csv'


def read_deliveries(name, year_to_match):
    id_list = list(read_matches(filename, year_to_match))
    bowlers = {}
    with open(name) as deliveries_file:
        deliveries_data = csv.DictReader(deliveries_file)
        for deliveries in deliveries_data:
            bowler_name = deliveries['bowler']
            runs_to_add = int(
                deliveries['total_runs'])-(int(deliveries['legbye_runs'])+int(deliveries['bye_runs']))
            super_over = deliveries['is_super_over']
            if deliveries['match_id'] in id_list:
                if super_over == "0":
                    if bowler_name not in bowlers:
                        bowlers[bowler_name] = {"runs": 0, "balls": 0}
                    bowlers[bowler_name]["runs"] += int(runs_to_add)
                    if deliveries['noball_runs'] == "0" and deliveries['wide_runs'] == "0":
                        bowlers[bowler_name]['balls'] += 1
                if len(bowlers) == 99:
                    return bowlers


bowler_details_2015 = read_deliveries(filename2, "2015")


def economy_rate_calculation():
    bowler_details = bowler_details_2015
    for bowler_, data in sorted(bowler_details.items()):
        if bowler_details[bowler_]['balls'] >= 6:
            overs = bowler_details[bowler_]['balls']/6
            bowler_details[bowler_]['economy'] = bowler_details[bowler_]['runs']/overs
        else:
            del bowler_details[bowler_]
    return bowler_details

# # find top 10 economic bowler


def top_10_economical_bowler():

    bowler_performance = economy_rate_calculation()
    bowler_economy_details = []
    for bowler, parameters in sorted(bowler_performance.items()):
        bowler_economy_details.append(bowler_performance[bowler]['economy'])
    bowler_economy_details.sort()
    bowler_economy_details = bowler_economy_details[:15]
    send_data_to_plot = {}
    for bowler, parameters in sorted(bowler_performance.items()):
        for name, economy in sorted(parameters.items()):
            if name == "economy" and economy in bowler_economy_details:
                send_data_to_plot[bowler] = economy
    return send_data_to_plot


def plot_data():
    economy_dictionary = top_10_economical_bowler()
    x_coordinate = []
    y_coordinate = []
    for name, economy in sorted(economy_dictionary.items(), key=lambda x: x[1]):
        y_coordinate.append(round(economy, 2))
        x_coordinate.append(name)
    plt.bar(x_coordinate, y_coordinate, width=0.5,
            color=['blue', 'red', 'green'])
    plt.xticks(rotation="45")
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.tight_layout()
    plt.show()


def execute():
    plot_data()


execute()
